# Experimental resources for P0014.3

Copyright 2015 Tessel Blom, Sebastiaan Mathôt, Chris Olivers, Stefan Van der Stigchel

- <s.mathot@cogsci.nl>
- <http://www.cogsci.nl/smathot>

# About this repository

This repository contains materials to accompany the following manuscript:

Blom, T., Mathôt, S., Olivers, C., Van der Stigchel, S. (in prep). *The pupillary light response reflects encoding, but not maintenance, in visual working memory*.

# Folder structure

- `experiment` contains the experiment files.
- `resources` contains the script for scaling the stimuli for Experiment 1.
- `data` contains the participant data and analysis scripts

# Dependencies

- OpenSesame 2.9.X for the experiments
	- http://osdoc.cogsci.nl/
- exparser for the analysis
	- https://github.com/smathot/exparser
- JASP for the analysis (only to `open bayes-model.jasp`)
	- http://www.jasp-stats.org/
- yamldoc for automatic validation of the analysis functions
	- https://github.com/smathot/python-yamldoc
- Python 2.7 and the standard scipy stack (numpy, matplotlib, etc.)

# Running the analysis

1. Convert all `data/edf/exp[X]/*.edf` to `.asc` files using the `edf2asc` tool provided by SR Research.
2. Move the `.asc` files to `data/data/exp[X]/*.asc`.
3. Open a terminal in `data`
4. Run `./full-analysis.sh`

This will produce a lot of output in the terminal, most of which is not terribly important. The main results will be written to two automatically created subfolders: `plot` and `output`

# License

- Analysis and experimental code are released under a [GNU General Public License 3](https://www.gnu.org/copyleft/gpl.html).
- Data and text are released under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
