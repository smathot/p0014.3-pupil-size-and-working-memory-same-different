#!/usr/bin/env python

import os
from scipy import misc, ndimage
from matplotlib import pyplot as plt
import numpy as np


goalSize = 40000
maxErr = 512
f = 1.001

for p in os.listdir('original'):

	print(p)
	a = ndimage.imread('original/'+p, flatten=True)
	_f = f
	_a = a[:]
	while True:
		i1 = np.where(a > 128)
		i2 = np.where(a <= 128)
		a[i1] = 255
		a[i2] = 0
		size = np.sum(a == 0)
		print(_f, size, a.shape)
		if abs(size - goalSize) < maxErr:
			break
		if size < goalSize:
			_f *= f
		else:
			_f /= f
		a = ndimage.zoom(_a, _f, cval=255)
		#if p == '4.png':
			#print(np.unique(a))
			#plt.imshow(a)
			#plt.show()
	misc.imsave('scaled/'+p, a)
