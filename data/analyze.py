#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
This file is part of P0014.1.

P0014.1 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

P0014.1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with P0014.1.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
from analysis import constants
if '--exp1' in sys.argv:
	constants.exp = 'exp1'
elif '--exp2' in sys.argv:
	constants.exp = 'exp2'
elif '--exp3' in sys.argv:
	constants.exp = 'exp3'
elif '--expX' in sys.argv:
	constants.exp = 'expX'
else:
	raise Exception('You must specify --exp1, --exp2, --exp3, or --expX')

from exparser import Tools
from analysis import helpers, parse, pupil, corr, crossExp, bayes

Tools.analysisLoop(
	parse.getDataMatrix(cacheId='data-%s' % constants.exp),
	mods=[helpers, pupil, corr, crossExp, bayes],
	pre=['filter'],
	full=['descriptives', 'behavior', 'pupilTracePlotSubject',
		'pupilTracePlotGrand', 'pupilTracePlotCorrect', 'corrPlot'],
	)
