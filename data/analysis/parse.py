#-*- coding:utf-8 -*-

"""
This file is part of P0014.1.

P0014.1 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

P0014.1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with P0014.1.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
from exparser.EyelinkAscFolderReader import EyelinkAscFolderReader
from exparser.Cache import cachedDataMatrix
from analysis.constants import *
import numpy as np

# Display center
xc = 512
yc = 384
trialId = 0

class MyReader(EyelinkAscFolderReader):

	"""An experiment-specific reader to parse the EyeLink data files."""

	def startTrial(self, l):

		global trialId
		if 'start_trial' in l:
			trialId += 1
			return trialId
		return None

	def initTrial(self, trialDict):

		"""
		desc:
			Performs pre-trial initialization.

		arguments:
			trialDict:
				desc:	Trial information.
				type:	dict
		"""

		trialDict['maxGazeErr'] = 0

	def finishTrial(self, trialDict):

		"""
		desc:
			Performs post-trial initialization.

		arguments:
			trialDict:
				desc:	Trial information.
				type:	dict
		"""

		sys.stdout.write('.')
		sys.stdout.flush()

	def parseLine(self, trialDict, l):

		"""
		desc:
			Parses a single line from the EyeLink .asc file.

		arguments:
			trialDict:
				desc:	Trial information.
				type:	dict
			l:
				desc:	A white-space-splitted line.
				type:	list
		"""

		if self.tracePhase is not None:
			if self.tracePhase in ['cue', 'memoryArray', 'retention',
				'testArray']:
				self.tracePhase = 'trial'

			fix = self.toFixation(l)
			if fix != None:
				trialDict['maxGazeErr'] = max(trialDict['maxGazeErr'],
					np.abs(xc-fix['x']))

@cachedDataMatrix
def getDataMatrix():
	global exp
	if exp == 'expX':
		exp = 'exp1'
		dm1 = getDataMatrix(cacheId='data-exp1')
		dm1['subject_nr'] += 1000
		dm1 = dm1.addField('exp', dtype=str, default='exp1')
		exp = 'exp2'
		dm2 = getDataMatrix(cacheId='data-exp2')
		dm2['subject_nr'] += 2000
		dm2 = dm2.addField('exp', dtype=str, default='exp2')
		exp = 'exp3'
		dm3 = getDataMatrix(cacheId='data-exp3')
		dm3['subject_nr'] += 3000
		dm3 = dm3.addField('exp', dtype=str, default='exp3')
		exp = 'expX'
		return dm1 + dm2 + dm3
	return MyReader(path='data/%s' % exp, blinkReconstruct=True).dataMatrix()
