#-*- coding:utf-8 -*-

"""
This file is part of P0014.1.

P0014.1 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

P0014.1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with P0014.1.  If not, see <http://www.gnu.org/licenses/>.
"""

from analysis.constants import *
from analysis import corr

def bayesModel(dm, traceParams=defaultTraceParams):

	"""
	desc:
		Creates the input file for the Bayesian model analysis, which is
		conducted in JASP.
	"""

	assert(exp == 'expX')
	pupilDiffTraceMem = corr.subjectDiffTraces(dm.select('cond == "memory"'),
		traceParams,
		cacheId='.subjectDiffTraces.memory.expX')
	pupilDiffTraceAtt = corr.subjectDiffTraces(dm.select('cond == "match"'),
		traceParams,
		cacheId='.subjectDiffTraces.attention.expX')
	pupilEncMem = nanmean(pupilDiffTraceMem[:, 3900:4000], axis=1)
	pupilMntMem = nanmean(pupilDiffTraceMem[:, 7900:8000], axis=1)
	pupilEncAtt = nanmean(pupilDiffTraceAtt[:, 3900:4000], axis=1)
	pupilMntAtt = nanmean(pupilDiffTraceAtt[:, 7900:8000], axis=1)
	a = np.array([
		['encMem'] + list(pupilEncMem),
		['mntMem'] + list(pupilMntMem),
		['encAtt'] + list(pupilEncAtt),
		['mntAtt'] + list(pupilMntAtt)
		], dtype=str)
	a = np.flipud(np.rot90(a))
	print(a)
	np.savetxt('output/bayesModel.csv', a, fmt='%s')
