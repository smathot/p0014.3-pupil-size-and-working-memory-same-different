#-*- coding:utf-8 -*-

"""
This file is part of P0014.1.

P0014.1 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

P0014.1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with P0014.1.  If not, see <http://www.gnu.org/licenses/>.
"""

from analysis.constants import *
from analysis import corr, helpers

def pupilBehavRegressSlice(dm, traceParams=defaultTraceParams):

	"""
	desc:
		Conducts a multiple-linear regression to investigate the relationship
		between accuracy and memory performance across the three experiments.

		Only uses the 3900-4000 slice. For the trace analysis, see
		pupilBehavRegress.

	arguments:
		dm:
			desc:	A DataMatrix.
			type:	DataMatrix

	keywords:
		traceParams:
			desc:	The pupil-trace parameters.
			type:	dict
	"""

	assert(exp == 'expX')
	dm = dm.select('cond == "memory"')
	N = dm.count('subject_nr')
	# Create an array with accuracies, and an array with experiment codes
	acc = np.empty(N, dtype=float)
	expCode = np.empty(N, dtype='S32')
	for i, (subject_nr, _dm) in enumerate(dm.walk('subject_nr')):
		acc[i] = 100.*_dm['correct'].mean()
		expCode[i] = _dm['exp'][0]
	# Create a 2d array with
	pupilDiffTrace = corr.subjectDiffTraces(dm, traceParams,
		cacheId='.subjectDiffTraces.memory.expX')
	# Walk through all samples and perform a regression
	import pandas as pd
	import statsmodels.formula.api as smf
	dv = 'pupil'
	pupil = nanmean(pupilDiffTrace[:, 3900:4000], axis=1)
	print(pupil, pupil.shape)
	d = {'acc' : acc, 'exp' : expCode, 'pupil' : pupil}
	df = pd.DataFrame(d)
	results = smf.ols('pupil ~ acc + exp', data=df).fit()
	print(results.summary())
	Plot.new(size=Plot.xs)
	plt.axhline(0, linestyle=':', color='black')
	plt.xlim(50, 100)
	plt.ylabel('Pupil-size difference (dark - bright)')
	plt.xlabel('Accuracy (%)')
	for _exp, color in [
		('exp1', green[1]),
		('exp2', purple[1]),
		('exp3', brown[1]),
		]:
		_df = df[df['exp']==_exp]

		x = _df['acc'].mean()
		xErr = 1.96*_df['acc'].std()/np.sqrt(len(_df))
		y = _df['pupil'].mean()
		yErr = 1.96*_df['pupil'].std()/np.sqrt(len(_df))
		plt.errorbar(x, y, xerr=xErr, yerr=yErr, color=color, fmt='o',
			capsize=0, label=_exp)
		plt.plot(_df['acc'], _df['pupil'], '.', color=color)
	xData = np.array([50, 100])
	yData1 = xData*results.params['acc'] + results.params['Intercept']
	yData2 = yData1 + results.params['exp[T.exp2]']
	yData3 = yData1 + results.params['exp[T.exp3]']
	plt.plot(xData, yData1, '--', color=green[1])
	plt.plot(xData, yData2, '--', color=purple[1])
	plt.plot(xData, yData3, '--', color=brown[1])
	plt.legend(frameon=False, loc='top left')
	Plot.save('multiLinRegress-slice', folder='expX')

def pupilBehavRegress(dm, traceParams=defaultTraceParams):

	"""
	desc:
		Conducts a multiple-linear regression to investigate the relationship
		between accuracy and memory performance across the three experiments.

	arguments:
		dm:
			desc:	A DataMatrix.
			type:	DataMatrix

	keywords:
		traceParams:
			desc:	The pupil-trace parameters.
			type:	dict
	"""

	assert(exp == 'expX')
	dm = dm.select('cond == "memory"')
	N = dm.count('subject_nr')
	# Create an array with accuracies, and an array with experiment codes
	acc = np.empty(N, dtype=float)
	expCode = np.empty(N, dtype='S32')
	for i, (subject_nr, _dm) in enumerate(dm.walk('subject_nr')):
		acc[i] = _dm['correct'].mean()
		expCode[i] = _dm['exp'][0]
	# Create a 2d array with
	pupilDiffTrace = corr.subjectDiffTraces(dm, traceParams,
		cacheId='.subjectDiffTraces.memory.expX')
	# Walk through all samples and perform a regression
	import pandas as pd
	import statsmodels.formula.api as smf
	lP2 = []
	lMin = []
	lMax = []
	lEst = []
	dv = 'pupil'
	for _i in range(0, pupilDiffTrace.shape[1], 1):
		if _i % 100 == 0:
			print(_i)
		pupil = pupilDiffTrace[:, _i]
		d = {'acc' : acc, 'exp' : expCode, 'pupil' : pupil}
		df = pd.DataFrame(d)
		results = smf.ols('acc ~ pupil + exp', data=df).fit()
		p2 = results.pvalues[dv]
		lP2.append(p2)
		lEst.append(results.params[dv])
		cdf = results.conf_int()
		_min = cdf[0][dv]
		lMin.append(_min)
		_max = cdf[1][dv]
		lMax.append(_max)
	Plot.new(size=Plot.w)
	helpers.annotate(dm, y=3.8)
	xData = np.arange(len(lMin))
	plt.fill_between(xData, lMin, lMax, alpha=.1, color=blue[1])
	plt.plot(lEst, color=blue[1])
	tk.markStats(plt.gca(), lP2, below=True, thr=.05)
	plt.axhline(0, color='black', linestyle=':')
	plt.ylabel('Pupil size - accuracy (slope)')
	Plot.save('multiLinRegress', folder='expX')

def crossExpCorrPlot(dm, traceParams=defaultTraceParams):

	"""
	desc:
		Creates a plot with seperate correlation traces for the memory condition
		of each expeirment.

	arguments:
		dm:
			desc:	A DataMatrix.
			type:	DataMatrix

	keywords:
		traceParams:
			desc:	The pupil-trace parameters.
			type:	dict
	"""

	Plot.new((6,4))
	dm = dm.select('cond == "memory"')
	helpers.annotate(dm, y=.9)
	plt.axhline(0, linestyle=':', color='black')
	plt.ylim(-.25, 1)
	for _exp in (1,2,3):
		_dm = dm.select('exp == "exp%d"' % _exp)
		ar, ap = corr.corrTrace(_dm, 'memory', _exp='exp%d' % _exp,
			example=False)
		plt.plot(ar, color=expColor[_exp], label='Exp %d' % _exp)
	plt.legend(frameon=False)
	plt.xlim(0, 8000)
	Plot.save('crossExpCorrTrace', folder='expX')

def crossExpLme(dm, traceParams=defaultTraceParams):

	"""
	desc:
		Conducts a cross-experimental lme.

	arguments:
		dm:
			desc:	A DataMatrix.
			type:	DataMatrix

	keywords:
		traceParams:
			desc:	The pupil-trace parameters.
			type:	dict
	"""

	assert(exp == 'expX')
	dm = dm.select('cond == "memory"')
	dm = dm.select('correct == 1')
	model = 'targetColor * exp + (1+targetColor+exp|subject_nr)'
	lm = tk.mixedModelTrace(dm, model, winSize=winSize, cacheId='crossExpLme',
		**traceParams)
	Plot.new(size=Plot.w)
	helpers.annotate(dm, y=3)
	plt.ylabel('T value')
	plt.axhline(0, color='black', linestyle='--')
	plt.axhline(-2, color='black', linestyle=':')
	plt.axhline(2, color='black', linestyle=':')
	tk.statsTrace(lm, show=False)
	Plot.save('crossExpLme', folder='expX')

def crossExpPupilPlot(dm, traceParams=defaultTraceParams):

	"""
	desc:
		Creates a pupil-size effect plot with the three experiments as
		separate lines.

	arguments:
		dm:
			desc:	A DataMatrix.
			type:	DataMatrix

	keywords:
		traceParams:
			desc:	The pupil-trace parameters.
			type:	dict
	"""

	assert(exp == 'expX')
	dm = dm.select('correct == 1')
	Plot.new(Plot.h)
	for i, cond in enumerate(('memory', 'match')):
		plt.subplot(2,1,i+1)
		if cond == 'memory':
			plt.title('a) Working-memory condition')
		else:
			plt.title('b) Attention condition')
		__dm = dm.select('cond == "%s"' % cond)
		helpers.annotate(__dm, y=.055, rot='horizontal')
		plt.axhline(0, linestyle=':', color='black')
		plt.ylim(-.01, .06)
		for _exp in (1,2,3):
			if cond == 'match' and _exp == 3:
				continue
			_dm = __dm.select('exp == "exp%d"' % _exp)
			plt.axhline(1, linestyle='--', color='black')
			plt.ylabel('Pupil-size difference (norm.)')
			tk.plotTraceContrast(_dm, q1, q2, labelDiff='Exp %d' % _exp,
				showAbs=False, showDiff=True, colorDiff=expColor[_exp],
				**traceParams)
		plt.xlim(0, 8000)
		plt.legend(frameon=False, loc='upper right')
	Plot.save('crossExpPupilPlot', folder='expX')
