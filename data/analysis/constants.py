#-*- coding:utf-8 -*-

"""
This file is part of P0014.1.

P0014.1 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

P0014.1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with P0014.1.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import math
from exparser import TraceKit as tk
from exparser import Plot
from exparser.TangoPalette import *
from exparser.Cache import cachedDataMatrix, cachedArray
from exparser.PivotMatrix import PivotMatrix
from yamldoc import validate
from matplotlib import pyplot as plt
import warnings
import numpy as np
from scipy.stats import linregress, nanmedian, nanmean

defaultTraceParams = {
	'signal'		: 'pupil',
	'phase'			: 'trial',
	'traceLen'		: 9000,
	'lock'			: 'start',
	'baseline'		: 'baseline',
	'baselineLock'	: 'end',
	'baselineLen'	: 100,
	}

show = '--show' in sys.argv
expColor = {
	1 : green[1],
	2 : purple[1],
	3 : brown[1],
	}
brightColor = orange[1]
darkColor = blue[1]
corrColor = green[2]
winSize = 10
yLim = .8, 1.1
q1 = 'targetColor == "white"'
q2 = 'targetColor == "black"'
