#-*- coding:utf-8 -*-

"""
This file is part of P0014.1.

P0014.1 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

P0014.1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with P0014.1.  If not, see <http://www.gnu.org/licenses/>.
"""

from analysis.constants import *

def filter(dm):

	"""
	desc:
		Filters and recodes the data for subsequent processing.

	arguments:
		dm:
			desc:	A DataMatrix.
			type:	DataMatrix

	returns:
		desc:	A DataMatrix.
		type:	DataMatrix
	"""

	if exp != 'expX':
		assert(dm.count('file') == dm.count('subject_nr'))
	dm = dm.select('practice == "no"')
	dm = dm.select('maxGazeErr < 100')
	dm = dm.select('cond != ""')
	dm = dm.select('response_time != ""')
	return dm

@validate
def descriptives(dm):

	"""
	desc:
		Provide some descriptives, such as cellcount, overall accuracy, etc.

	arguments:
		dm:
			desc:	A DataMatrix.
			type:	DataMatrix
	"""

	pm = PivotMatrix(dm, ['subject_nr'], ['subject_nr'], dv='correct',
		func='size')
	pm._print('N')

@validate
def behavior(dm):

	"""
	desc:
		Analyzes accuracy and response times.

	arguments:
		dm:
			desc:	A DataMatrix.
			type:	DataMatrix
	"""

	pm = PivotMatrix(dm, ['cond', 'targetColor'], ['subject_nr'], dv='correct')
	pm._print('Accuracy')
	pm.save('output/%s/correct.csv' % exp)
	pm = PivotMatrix(dm, ['cond', 'targetColor'], ['subject_nr'],
		dv='response_time')
	pm._print('Response times')
	pm.save('output/%s/response_time.csv' % exp)

def annotate(dm, dx=200, rot='vertical', y=1.08, resp=False):
	
	"""
	desc:
		Annotate the current plot with vertical aligns to indicate the epochs,
		etc.
		
	arguments:
		dm:		A DataMatrix.
		
	keywords:
		dx:		The displacement of the epoch labels relative to the epoch
				lines.
		rot:	The orientation of the epoch labels (horizontal or vertical)
		y:		The Y position of the epoch labels.
		resp:	Indicates whether the response interval should be included.
	"""		

	plt.axvline(1000, linestyle=':', color='black')
	plt.axvline(4000, linestyle=':', color='black')
	if resp:
		plt.axvline(8000, linestyle=':', color='black')
		plt.xlim(0, 9000)
	else:
		plt.xlim(0, 8000)
	plt.xticks(np.linspace(0, 9000, 10), np.arange(0, 10))
	plt.xlabel('Time since cue onset (s)')
	if dm == 'memory' or dm['cond'][0] == 'memory':
		plt.text(dx, y, 'Cue', rotation=rot)
		plt.text(1000+dx, y, 'Memory array', rotation=rot)
		plt.text(4000+dx, y, 'Retention', rotation=rot)
		if resp:
			plt.text(8000+dx, y, 'Response', rotation=rot)
	else:
		plt.text(dx, y, 'Target', rotation=rot)
		plt.text(1000+dx, y, 'Search array', rotation=rot)
		plt.text(4000+dx, y, 'Passive wait', rotation=rot)
		if resp:
			plt.text(8000+dx, y, 'Response', rotation=rot)
