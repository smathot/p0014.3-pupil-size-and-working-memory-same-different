#-*- coding:utf-8 -*-

"""
This file is part of P0014.1.

P0014.1 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

P0014.1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with P0014.1.  If not, see <http://www.gnu.org/licenses/>.
"""

from analysis.constants import *
from analysis.helpers import annotate

@validate
def pupilTracePlot(dm, suffix='', model=None, traceParams=defaultTraceParams):
	
	"""
	desc:
		The base function for creating a single pupil-trace plot.
		
	arguments:
		dm:		A DataMatrix.
		
	keywords:
		suffix:	A suffix for the cache.
		model:	A statistical model, or None for no model.
		
	kwdict:
		traceParams:	The trace parameters.
	"""

	cols = ['targetColor', 'cond', 'subject_nr', '__trace_trial__',
		'__trace_baseline__']
	_dm = dm.selectColumns(cols)
	annotate(dm)
	plt.ylabel('Pupil size (norm.)')
	plt.axhline(1, linestyle='--', color='black')
	tk.plotTraceContrast(_dm, q1, q2, model=model, color1=brightColor,
		color2=darkColor,
		label1='White (N=%d)' % len(_dm.select(q1)),
		label2='Black (N=%d)' % len(_dm.select(q2)),
		winSize=winSize, cacheId='.lmerPupil%s.%s' % (suffix, exp), **traceParams)
	plt.legend(frameon=False, loc='lower right')

@validate
def pupilTracePlotCond(dm, traceParams=defaultTraceParams):
	
	"""
	desc:
		Plots the pupil trace during the retention interval, separately for both
		conditions (memory and attention), but across brightness.

	arguments:
		dm:
			desc:	A DataMatrix.
			type:	DataMatrix

	keywords:
		traceParams:
			desc:	The pupil-trace parameters.
			type:	dict
	"""	

	cols = ['targetColor', 'cond', 'subject_nr', '__trace_trial__',
		'__trace_baseline__']
	dm = dm.select('correct == 1')
	model = 'cond + (1+cond|subject_nr)'
	_dm = dm.selectColumns(cols)
	Plot.new(Plot.r)
	plt.axhline(1, linestyle='--', color='black')
	q1 = 'cond == "match"'
	q2 = 'cond == "memory"'
	tk.plotTraceContrast(_dm, q1, q2, model=model,
		label1='Match (N=%d)' % len(_dm.select(q1)),
		label2='Memory (N=%d)' % len(_dm.select(q2)),
		winSize=winSize, cacheId='.lmerPupil.cond.%s' % exp, **traceParams)
	plt.legend(frameon=False, loc='upper right')
	Plot.save('pupilTracePlotCond', folder=exp, show=show)

@validate
def pupilTracePlotOrder(dm, traceParams=defaultTraceParams):
	
	"""
	desc:
		Plots the pupil trace during the retention interval, separately for
		both orders of the counterbalancing. (I.e. memory first or attention
		first.)

	arguments:
		dm:
			desc:	A DataMatrix.
			type:	DataMatrix

	keywords:
		traceParams:
			desc:	The pupil-trace parameters.
			type:	dict
	"""	

	model = 'targetColor + (1+targetColor|subject_nr)'
	dm = dm.select('correct == 1')
	dm = dm.select('cond == "memory"')
	Plot.new(Plot.h)
	plt.subplot(2, 1, 1)
	_dm = dm.select('cond1 == "memory"')
	plt.title('All subjects (memory first; N=%d)' % len(_dm))
	pupilTracePlot(_dm, suffix='.memoryFirst', model=model)
	plt.ylim(yLim)
	plt.subplot(2, 1, 2)
	_dm = dm.select('cond1 == "match"')
	plt.title('All subjects (memory second; N=%d)' % len(_dm))
	pupilTracePlot(_dm, suffix='.memorySecond', model=model)
	plt.ylim(yLim)
	Plot.save('pupilTracePlotOrder', folder=exp, show=show)

	model = 'targetColor*count_block_sequence + (1+targetColor+count_block_sequence|subject_nr)'
	mm = tk.mixedModelTrace(dm, model=model, winSize=winSize,
		cacheId='.orderInteraction.%s' % exp, **defaultTraceParams)
	Plot.new(Plot.r)
	_mm = mm.select('effect == "count_block_sequence"')
	plt.plot(_mm['t'], label='Cond')
	_mm = mm.select('effect == "targetColorwhite:count_block_sequence"')
	plt.plot(_mm['t'], label='Interaction')
	plt.legend()
	Plot.save('statsTraceOrder', folder=exp, show=show)

@validate
def pupilTracePlotGrand(dm, traceParams=defaultTraceParams):

	"""
	desc:
		Plots the pupil trace during the retention interval, separately for cue-
		on-bright and cue-on-dark trials.

	arguments:
		dm:
			desc:	A DataMatrix.
			type:	DataMatrix

	keywords:
		traceParams:
			desc:	The pupil-trace parameters.
			type:	dict
	"""

	model = 'targetColor + (1+targetColor|subject_nr)'
	dm = dm.select('correct == 1')
	Plot.new(Plot.h)
	plt.subplot(2, 1, 1)
	plt.title('a) Working-memory condition')
	_dm = dm.select('cond == "memory"')
	# plt.title('All subjects (memory; N=%d)' % len(_dm))
	pupilTracePlot(_dm, suffix='.memory', model=model)
	plt.ylim(yLim)
	plt.xlim(0, 8000)
	plt.subplot(2, 1, 2)
	_dm = dm.select('cond == "match"')
	# plt.title('All subjects (match; N=%d)' % len(_dm))
	plt.title('b) Attention condition')
	pupilTracePlot(_dm, suffix='.match', model=model)
	plt.ylim(yLim)
	plt.xlim(0, 8000)
	Plot.save('pupilTracePlotGrand', folder=exp, show=show)

	model = 'targetColor*cond + (1+targetColor+cond|subject_nr)'
	mm = tk.mixedModelTrace(dm, model=model, winSize=winSize,
		cacheId='.grandInteraction.%s' % exp, **defaultTraceParams)
	Plot.new(Plot.r)
	_mm = mm.select('effect == "condmemory"')
	plt.plot(_mm['t'], label='Cond')
	_mm = mm.select('effect == "targetColorwhite:condmemory"')
	plt.plot(_mm['t'], label='Interaction')
	plt.legend(frameon=False)
	Plot.save('statsTraceGrand', folder=exp, show=show)

@validate
def pupilTracePlotSubject(dm, traceParams=defaultTraceParams):

	"""
	desc:
		Plots the pupil trace during the retention interval, separately for cue-
		on-bright and cue-on-dark trials, and separately for each participant.

	arguments:
		dm:
			desc:	A DataMatrix.
			type:	DataMatrix

	keywords:
		traceParams:
			desc:	The pupil-trace parameters.
			type:	dict
	"""

	dm = dm.select('correct == 1')
	N = dm.count('subject_nr')
	i = 1
	Plot.new((10,30))
	for subject_nr in dm.unique('subject_nr'):
		_dm = dm.select('subject_nr == %d' % subject_nr)
		plt.subplot(N, 2, i)
		__dm = _dm.select('cond == "match"')
		plt.title('subject %d (match; N=%d)' % (subject_nr, len(__dm)))
		pupilTracePlot(__dm)
		i += 1
		plt.subplot(N, 2, i)
		__dm = _dm.select('cond == "memory"')
		plt.title('subject %d (memory; N=%d)' % (subject_nr, len(__dm)))
		pupilTracePlot(__dm)
		i += 1
	Plot.save('pupilTracePlotSubject', folder=exp, show=show)

@validate
def pupilTracePlotCorrect(dm, traceParams=defaultTraceParams):

	"""
	desc:
		Plots the pupil trace during the retention interval, separately for cue-
		on-bright and cue-on-dark trials, and separately for correct and
		incorrect trials.

	arguments:
		dm:
			desc:	A DataMatrix.
			type:	DataMatrix

	keywords:
		traceParams:
			desc:	The pupil-trace parameters.
			type:	dict
	"""

	for cond in ('memory', ):
		_dm = dm.select('cond == "%s"' % cond)
		model = 'targetColor*correct + (1+targetColor+correct|subject_nr)'
		mm = tk.mixedModelTrace(_dm, model=model, winSize=winSize,
			cacheId='.correctInteraction.%s.%s' % (cond, exp),
			**defaultTraceParams)
		Plot.new(Plot.r)
		_mm = mm.select('effect == "targetColorwhite"')
		plt.plot(_mm['t'], label='Target color')
		_mm = mm.select('effect == "targetColorwhite:correct"')
		plt.plot(_mm['t'], label='Interaction')
		_mm = mm.select('effect == "correct"')
		plt.plot(_mm['t'], label='correct')
		plt.legend(frameon=False)
		Plot.save('statsTraceCorrect.%s' % cond, folder=exp, show=show)

	Plot.new(Plot.xl)
	i = 1
	model = 'targetColor + (1+targetColor|subject_nr)'
	for correct in (0,1):
		_dm = dm.select('correct == %d' % correct)
		plt.subplot(2, 2, i)
		plt.ylim(yLim)
		__dm = _dm.select('cond == "match"')
		plt.title('Correct: %d (match; N=%d)' % (correct, len(__dm)))
		if correct == 0:
			pupilTracePlot(__dm)
		else:
			pupilTracePlot(__dm, model=model, suffix='.match.correct%d' % correct)
		i += 1
		plt.subplot(2, 2, i)
		plt.ylim(yLim)
		__dm = _dm.select('cond == "memory"')
		plt.title('Correct %d (memory; N=%d)' % (correct, len(__dm)))
		pupilTracePlot(__dm, model=model, suffix='.memory.correct%d' % correct)
		i += 1
	Plot.save('pupilTracePlotCorrect', folder=exp, show=show)

def pupilTracePlotBrightness(dm):

	assert(exp == 'exp3')
	_dm = dm.select('')
	pupilTracePlot(_dm)
	
