#-*- coding:utf-8 -*-

"""
This file is part of P0014.1.

P0014.1 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

P0014.1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with P0014.1.  If not, see <http://www.gnu.org/licenses/>.
"""

from analysis.constants import *
from analysis.helpers import annotate

@cachedArray
def subjectDiffTraces(dm, traceParams=defaultTraceParams):

	dm = dm.select('correct == 1')
	pupil = np.empty( (dm.count('subject_nr'), traceParams['traceLen']),
		dtype=float)
	for i, (subject_nr, _dm) in enumerate(dm.walk('subject_nr')):
		x1, y1, err1 = tk.getTraceAvg(_dm.select(q1, verbose=False),
			**traceParams)
		x2, y2, err2 = tk.getTraceAvg(_dm.select(q2, verbose=False),
			**traceParams)
		pupil[i] = y2-y1
	return pupil

def corrTrace(dm, cond, _exp=exp, example=True, traceParams=defaultTraceParams):

	dm = dm.select('cond == "%s"' % cond)
	behav = np.empty(dm.count('subject_nr'), dtype=float)
	for i, (subject_nr, _dm) in enumerate(dm.walk('subject_nr')):
		behav[i] = _dm['correct'].mean()
	pupil = subjectDiffTraces(dm, traceParams,
		cacheId='.subjectDiffTraces.%s.%s' % (cond, _exp))
	ar = np.empty(traceParams['traceLen'], dtype=float)
	ap = np.empty(traceParams['traceLen'], dtype=float)
	for _i in range(pupil.shape[1]):
		s, i, r, p, se = linregress(behav, pupil[:,_i])
		ar[_i] = r
		ap[_i] = p
	# Create an example plot for the strongest correlation
	iMax = np.argmax(ar)
	x = behav
	y = pupil[:,iMax]
	if example:
		Plot.new(size=Plot.xs)
		Plot.regress(x, y, symbol='o')
		plt.xlabel('Accuracy (prop.)')
		plt.ylabel('Pupil-size difference (norm.)')
		plt.title('Sample: %d' % iMax)
		Plot.save('corrExample', folder=exp)
	return ar, ap

def corrPlot(dm, traceParams=defaultTraceParams):

	ar, ap = corrTrace(dm, 'memory')
	Plot.new(Plot.r)
	plt.subplot(211)
	annotate('memory', y=.9)
	plt.ylabel('R(pupil - accuracy)')
	tk.markStats(plt.gca(), ap, below=True, thr=.05)
	plt.plot(ar, color=corrColor, label='memory')
	plt.axhline(0, linestyle=':', color='black')
	plt.ylim(-.25,1)
	plt.subplot(212)
	plt.xlabel('Time (ms)')
	plt.ylabel('P value for R')
	plt.axhline(.05, linestyle=':', color='black')
	plt.plot(ap, color=blue[1], label='memory')
	plt.legend(frameon=False)
	Plot.save('corrTrace',  folder=exp, show=show)
