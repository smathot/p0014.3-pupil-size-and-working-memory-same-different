#!/bin/bash
rm -Rf plot/ output/
python analyze.py --exp1 @full | tee exp1.log
python analyze.py --exp2 @full | tee exp2.log
python analyze.py --exp3 @full | tee exp3.log
python analyze.py --expX @pupilBehavRegressSlice @crossExpLme @crossExpPupilPlot \
	@crossExpCorrPlot | tee expX.log
